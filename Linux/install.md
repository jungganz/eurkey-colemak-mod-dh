# EurKEY Colemak Mod-DH (Linux)

## How to install

1. make a backup of the base.extras.xml, evdev.extras.xml, eu files of your system
    - `cp /usr/share/X11/xkb/rules/base.extras.xml`
    - `cp /usr/share/X11/xkb/rules/evdev.extras.xml`
    - `cp /usr/share/X11/xkb/symbols/eu`
2. download the repository or just the Linux folder.
3. unzip the file
4. navigate in the unziped folder to /Linux/xkb/rules/base.extras.xml and copy the content
5. open /usr/share/X11/xkb/rules/base.extras.xml in an editor with root rights, search for the EurKEY entry (STRG+F) and paste the content between `</configItem>` and `</layout>`. That should result in a entry like in the end of the document and save it
6. Step 4 and 5 should be repeated, for xkb/rules/evdev.extras.xml and result in a similar entry as above
7. navigate in the unziped folder to /Linux/xkb/rules/base.extras.xml and copy the content
8. open /usr/share/X11/xkb/symbols/eu in an editor with root rights, and paste it at the of the document and save it.
9. restart the pc
10. In your keyboard/locale settings you should now find the EurKEY Colemak layout(s) besides the default EurKEY layout.
11. if everything works fine, you could now delete your backup and the downloaded files
    - `rm base.extras.xml`
    - `rm evdev.extras.xml`
    - `rm eu`
12. if anything doesn't work, you can just restore the backup by the following codelines, delete the backup -> Step 11 and restart your pc
    - `sudo mv base.extras.xml /usr/share/X11/xkb/rules/base.extras.xml`
    - `sudo mv evdev.extras.xml /usr/share/X11/xkb/rules/evdev.extras.xml`
    - `sudo mv eu /usr/share/X11/xkb/symbols/eu`

_The_ `AltGr+¬` _and the_ `AltGr+Shift+α` _layer is not implemented yet but you can add them by creating a the file /home/username/.XCompose and paste there the content of /Linux/XCompose of the downloaded folder. But that is not an elegant way and might affect your other installed Keyboard layouts as well._


## Example...
... of how the EurKEY entry of base.extras.xml and evdev.extras.xml should finally look like
```
      ` ` <shortDesc` is not implemented yet but you can add them by ri` is not implemented yet but you can add them by ption>eu</shortDescription>
        <description>EurKEY (US based layout with European letters)</description>
          <languageList>
            <iso639Id>cat</iso639Id>
            <iso639Id>dan</iso639Id>
            <iso639Id>eng</iso639Id>
            <iso639Id>est</iso639Id>
            <iso639Id>fao</iso639Id>
            <iso639Id>fin</iso639Id>
            <iso639Id>ger</iso639Id>
            <iso639Id>gre</iso639Id>
            <iso639Id>gsw</iso639Id>
            <iso639Id>ita</iso639Id>
            <iso639Id>lav</iso639Id>
            <iso639Id>lit</iso639Id>
            <iso639Id>nld</iso639Id>
            <iso639Id>nor</iso639Id>
            <iso639Id>por</iso639Id>
            <iso639Id>spa</iso639Id>
            <iso639Id>swe</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>eurkey-cmk-dh-ansi</name>
            <description>EurKEY (Colemak-DH, ANSI)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>eurkey-cmk-dh-iso</name>
            <description>EurKEY (Colemak-DH, ISO)</description>
          </configItem>
        </variant>
      </variantList>
    </layout>
```